#[macro_use]
extern crate gstreamer;

extern crate failure;
use failure::Error;

#[macro_use]
extern crate failure_derive;

#[derive(Debug, Fail)]
#[fail(display = "Missing element {}", _0)]
struct MissingElement(&'static str);

#[derive(Debug, Fail)]
#[fail(
    display = "Received error from {}: {} (debug: {:?})",
    src, error, debug
)]
struct ErrorMessage {
    src: String,
    error: String,
    debug: Option<String>,
    #[cause]
    cause: glib::Error,
}


use std::sync::{mpsc};


use gl_render::{clear, load_gl, gl};
use gl_render::{Texture, VertexObject, RenderableObject, Uniformable};//, TextureShaderObject};


//hier kommt jetzt der ganze gestreamer quatsch
use gstreamer::prelude::*;
use gstreamer_gl::prelude::*;

use glutin;
//use glutin::dpi::LogicalSize;

//use glutin::event::VirtualKeyCode;
use glutin::event_loop::EventLoop;
use glutin::window::{WindowBuilder};


const SIMPLE_FRAG_SHADER: &str = r##"
#version 150

in vec2 v_texcoord;
out vec4 Target0;
uniform sampler2D texture1;

void main() {
    Target0 = texture(texture1, v_texcoord * vec2(1.0, 1.0));
}
"##;

const SIMPLE_VERT_SHADER: &str = r##"
#version 150

uniform mat4 u_transformation;
in vec4 a_position;
in vec2 a_texcoord;
out vec2 v_texcoord;

void main() {
    gl_Position = u_transformation * a_position;
    v_texcoord = a_texcoord;
}
"##;


pub struct ExampleRenderer<R: RenderableObject<gl::Gl>> {
    render: R,
    texture: Texture,
}

impl ExampleRenderer<VertexObject> {

    pub fn new(gl: &gl::Gl) -> ExampleRenderer<VertexObject> {

        let frag = SIMPLE_FRAG_SHADER;
        let vert = SIMPLE_VERT_SHADER;

        let render = VertexObject::new()
            .set_shader(gl, frag.to_string() , vert.to_string())
            .build(gl);

        let texture = Texture::from_image(gl, "./test.jpg").unwrap();

        ExampleRenderer {
            render,
            texture,
        }
    }

    pub fn render(&mut self) {


        self.render.begin_render();
        self.render.activate_texture(self.texture.id(), 0);
        self.render.set_uniform("texture1", 0);

        //self.render.activate_texture(next_id, 1);
        //self.render.set_uniform("to", 1);

        //self.render.set_uniform("progress", 0.0);
        //self.render.set_uniform("flip_from", self.item.flip());
        //self.render.set_uniform("flip_to", to_flip);

        self.render.end_render();

    }

    pub fn set_texture(&mut self, texture_id: Option<u32>) {
        if let Some(id) = texture_id {
            self.texture.set_id(id);
        }
    }

    /*pub fn resize(&mut self, width: f32, height: f32) {
        self.item.resize(width, height);
        self.only_one_time_scope = false;
    }*/

    /*pub fn keypress(&mut self, key: glutin::event::VirtualKeyCode) {

        match key {
            glutin::event::VirtualKeyCode::P => {
                self.item.in_scope();
            },
            glutin::event::VirtualKeyCode::O => {
                self.item.begin_go_out_of_scope();
            },
            _ => {}
        }
        self.item.keypress(key);
        

    }*/
}



fn main() {

    if let Err(e) = gstreamer::init() {
        panic!("Error on GST_INIT {:#?}", e);
    }

    let event_loop = EventLoop::new();
    let window = WindowBuilder::new().with_title("Blank Window Example");


    let windowed_context = glutin::ContextBuilder::new()
        .with_multisampling(2)
        .with_vsync(true)
        .with_stencil_buffer(2)
        .with_double_buffer(Some(true))
        .with_hardware_acceleration(Some(true))
        .build_windowed(window, &event_loop)
        .unwrap();

    let windowed_context = unsafe { windowed_context.make_current().unwrap() };
    let gl = load_gl(&windowed_context);


    // hier machen wir mal das init für das example

    let mut example_renderer = ExampleRenderer::new(&gl);



    // video shit
    use glutin::platform::unix::RawHandle; // should only work on linux systems
    use glutin::platform::unix::WindowExtUnix;
    use glutin::platform::ContextTraitExt;

    let pipeline = create_pipeline("./test.mp4".to_string());
    if let Err(e) = pipeline {
        panic!("Error on Create Pipeline {:#?}", e);
    }

    let (pipeline, appsink, glupload) = pipeline.unwrap();

    let bus = pipeline
            .bus()
            .expect("Pipeline without bus. Shouldn't happen!");

    let window = &windowed_context.window();
    let context = &windowed_context.context();

    let shared_context: gstreamer_gl::GLContext;

    let api = gstreamer_gl::GLAPI::OPENGL3;

    let (gl_context, gl_display, platform): (usize, gstreamer_gl::GLDisplay, gstreamer_gl::GLPlatform) = match unsafe { context.raw_handle() } {
                
        RawHandle::Glx(glx_context) => {
            let gl_display = if let Some(display) = window.xlib_display() {
                unsafe { gstreamer_gl_x11::GLDisplayX11::with_display(display as usize) }.unwrap()
            } else {
                panic!("X11 window without X Display");
            };

            println!("what is the clear gldipsplay? {:?}", gl_display);

            (
                glx_context as usize,
                gl_display.upcast::<gstreamer_gl::GLDisplay>(),
                gstreamer_gl::GLPlatform::GLX,
            )
        }
        handler => panic!("Unsupported platform: {:?}.", handler),

    };

    println!("gl_context: {:?}", gl_context);
    println!("gl_display: {:?}", gl_display);
    println!("platform: {:?}", platform);

    shared_context =
        unsafe { 
            let l = gstreamer_gl::GLContext::new_wrapped(&gl_display, gl_context, platform, api);
            println!("state of new wrapped context: {:?}", l);
            l.unwrap()
        };
            

    shared_context
        .activate(true)
        .expect("Couldn't activate wrapped GL context");

    let r = shared_context.fill_info();

    if let Err(e) = r {
        println!("error on fill info with gl context from current thread: {}", e);
    }


    let gl_context = shared_context.clone();

    #[allow(clippy::single_match)]
    bus.set_sync_handler(move |_, msg| {
        use gstreamer::MessageView;

        match msg.view() {
            MessageView::NeedContext(ctxt) => {
                let context_type = ctxt.context_type();
                if context_type == *gstreamer_gl::GL_DISPLAY_CONTEXT_TYPE {
                    if let Some(el) =
                        msg.src().map(|s| s.downcast::<gstreamer::Element>().unwrap())
                    {
                        let context = gstreamer::Context::new(context_type, true);
                        context.set_gl_display(&gl_display);
                        el.set_context(&context);
                    }
                }
                if context_type == "gst.gl.app_context" {
                    if let Some(el) =
                        msg.src().map(|s| s.downcast::<gstreamer::Element>().unwrap())
                    {
                        let mut context = gstreamer::Context::new(context_type, true);
                        {
                            let context = context.get_mut().unwrap();
                            let s = context.structure_mut();
                            s.set_value("context", gl_context.to_send_value());
                        }
                        el.set_context(&context);
                    }
                }
            }
            _ => (),
        }
        gstreamer::BusSyncReply::Pass
    });


    let video_setup = setup(&appsink);
    if let Err(e) = video_setup {
        panic!("Error on Video::setup {:#?}", e);
    }

    let receiver = video_setup.unwrap();


    if let Err(e) = pipeline.set_state(gstreamer::State::Playing) {
        println!("Error on set pipeline state to Playing on fn play: {:?}", e);
    }


    use glutin::event::Event;
    use glutin::event::WindowEvent;
    use glutin::event_loop::ControlFlow;

    event_loop.run(move |event, _, control_flow| {
        match event {
            Event::LoopDestroyed => return,
            Event::WindowEvent { event, .. } => match event {
                WindowEvent::CloseRequested => {
                    *control_flow = ControlFlow::Exit;
                    println!("exit close request");
                }
                WindowEvent::KeyboardInput {
                    input:
                        glutin::event::KeyboardInput {
                            state: glutin::event::ElementState::Pressed,
                            virtual_keycode: key,
                            ..
                        },
                    ..
                } => {
                    if key == Some(glutin::event::VirtualKeyCode::Escape) {
                        *control_flow = ControlFlow::Exit;
                        println!("esc exiting");
                    }
                }
                WindowEvent::Resized(physical_size) => {
                    windowed_context.resize(physical_size);
                }
                _ => (),
            },
            Event::MainEventsCleared => {
                //windowed_context.window().request_redraw();
            }
            Event::RedrawRequested(_) => {
                clear(&gl);
                example_renderer.render();
                windowed_context.swap_buffers().unwrap();
            }
            _ => *control_flow = ControlFlow::Poll,
        };

        let mut curr_frame: Option<gstreamer_video::VideoFrame<gstreamer_video::video_frame::Readable>> = None;

        if let Some(sample) = receiver.try_iter().last() {
            let buffer = sample.buffer_owned().unwrap();
            let info = sample
                .caps()
                .and_then(|caps| gstreamer_video::VideoInfo::from_caps(caps).ok())
                .unwrap();

            if let Ok(frame) = gstreamer_video::VideoFrame::from_buffer_readable_gl(buffer, &info) {


                curr_frame = Some(frame);
            }
    
        }

        if let Some(frame) = curr_frame.as_ref() {
            // set a new texture id...
            //return frame.get_texture_id(0);
            clear(&gl);
            //let sync_meta = frame.buffer().meta::<gstreamer_gl::GLSyncMeta>().unwrap();
            //sync_meta.wait(&shared_context);

            example_renderer.set_texture(frame.texture_id(0));
            example_renderer.render();
            windowed_context.swap_buffers().unwrap();
        }
    });


}



/// setup - create a thread and mpsc receive channel for frame and video processing
fn setup(appsink: &gstreamer_app::AppSink) -> Result<mpsc::Receiver<gstreamer::Sample>, Error> {

    let (sender, receiver) = mpsc::channel();
    /*let sender_clone = Mutex::new(sender.clone());*/
    
    appsink.set_callbacks(
        gstreamer_app::AppSinkCallbacks::builder()
            .new_sample(move |appsink| {
                let sample = appsink.pull_sample().map_err(|_| gstreamer::FlowError::Eos)?;

                sender
                    .send(sample)
                    .map(|_| gstreamer::FlowSuccess::Ok)
                    .map_err(|_| gstreamer::FlowError::Error);

                // liegt der memory leak am sender_clone? (lock // unwrap)?
                /*
                sender_clone
                    .lock()
                    .unwrap()
                    .send(sample)
                    .map(|_| gst::FlowSuccess::Ok)
                    .map_err(|_| gst::FlowError::Error).unwrap(); // do unwrap and add OK Flowsuccess..
                */
                Ok(gstreamer::FlowSuccess::Ok)    
            })
            .build(),
    );

    Ok(receiver)
}




fn create_pipeline(path: String) -> Result<(gstreamer::Pipeline, gstreamer_app::AppSink, gstreamer::Element), Error> {
    use gstreamer::ElementFactory;
    
    let pipeline = gstreamer::Pipeline::new(None);
    
    let src = ElementFactory::make("filesrc", None)
        .map_err(|_| MissingElement("filesrc"))?;

    let demuxer = ElementFactory::make("matroskademux", None)
        .map_err(|_| MissingElement("matroskademux"))?;

    pipeline.add_many(&[&src, &demuxer])?;
    src.link(&demuxer)?;


    let queue = ElementFactory::make("queue", None).map_err(|_| MissingElement("queue"))?;

    queue.set_property("max-size-buffers", &(100u32 * 30u32))?;
    queue.set_property("max-size-bytes", &(2097152u32 * 30u32))?;
    queue.set_property("max-size-time", &(2000000000u64 * 30u64))?;


    let decoder = {
        if let Ok(d) = ElementFactory::make("vaapidecodebin", None) {
            d.set_property("disable-vpp", &true)?;
            d
        } else {
            println!("cant find vaapidecodebin, use avdec_vp8");
            ElementFactory::make("avdec_vp8", None).map_err(|_| MissingElement("avdec_vp8"))?
        }
    };




    let sink = ElementFactory::make("glsinkbin",None).map_err(|_| MissingElement("glsinkbin"))?;

    let elements = &[&queue, &decoder, &sink];
    pipeline.add_many(elements)?;
    gstreamer::Element::link_many(elements)?;


    // set the video path
    src.set_property("location", &path)?;

    //hmmm did i need it?
    for e in elements {
        e.sync_state_with_parent()?
    }

    let queue_weak = queue.downgrade();
    let pipeline_weak = pipeline.downgrade();
    demuxer.connect_pad_added(move |demux, src_pad| {

        let queue = match queue_weak.upgrade() {
            Some(queue) => queue,
            None => return,
        };

        let pipeline = match pipeline_weak.upgrade() {
            Some(pipeline) => pipeline,
            None => return,
        };

        handle_demux_pad_added(demux, src_pad, &queue, &pipeline)
    });

    demuxer
        .sync_state_with_parent()
        .expect("Failed to build remux pipeline");

    let appsink = ElementFactory::make("appsink", None)
        .map_err(|_| MissingElement("appsink"))?
        .dynamic_cast::<gstreamer_app::AppSink>()
        .expect("Sink element is expected to be an appsink!");

    sink.set_property("sink", &appsink)?;

    appsink.set_property("enable-last-sample", &false)?;
    appsink.set_property("emit-signals", &false)?;
    //appsink.set_property("max-buffers", &1u32.to_value())?;

    let caps = gstreamer::Caps::builder("video/x-raw")
        .features(&[&gstreamer_gl::CAPS_FEATURE_MEMORY_GL_MEMORY])
        .field("format", &gstreamer_video::VideoFormat::Rgba.to_string())
        .field("texture-target", &"2D")
        .build();
    appsink.set_caps(Some(&caps));

    // get the glupload element to extract later the used context in it
    let mut iter = sink.dynamic_cast::<gstreamer::Bin>().unwrap().iterate_elements();
    let glupload = loop {
        match iter.next() {
            Ok(Some(element)) => {

                if "glupload" == element.factory().unwrap().name() {
                    break Some(element);
                }

            }
            Err(gstreamer::IteratorError::Resync) => iter.resync(),
            _ => break None,
        }
    };

    Ok((pipeline, appsink, glupload.unwrap()))


}


// This is the callback function called by the demuxer, when a new stream was detected.
fn handle_demux_pad_added(
    demuxer: &gstreamer::Element,
    src_pad: &gstreamer::Pad,
    videoqueue: &gstreamer::Element,
    pipeline: &gstreamer::Pipeline,
) {
    use gstreamer::ElementFactory;
    //println!("what is the name of the src_pad:{}", src_pad.get_name().as_str());

    let (is_audio, is_video) = {
        let media_type = src_pad.current_caps().and_then(|caps| {
            caps.structure(0).map(|s| {
                let name = s.name();
                (name.starts_with("audio"), name.starts_with("video"))
            })
        });

        match media_type {
            None => {
                /*gst_element_warning!(
                    demuxer,
                    gst::CoreError::Negotiation,
                    ("Failed to get media type from pad {}", src_pad.get_name())
                );*/

                return;
            }
            Some(media_type) => media_type,
        }
    };


    // We create a closure here, calling it directly below it, because this greatly
    // improves readability for error-handling. Like this, we can simply use the
    // ?-operator within the closure, and handle the actual error down below where
    // we call the insert_sink(..) closure.
    let insert_sink = |is_audio, is_video| -> Result<(), Error> {
        if is_audio {
            let queue = {
                if let Some(queue) = pipeline.by_name("audio_queue") {
                    queue
                } else {
                    // decodebin found a raw audiostream, so we build the follow-up pipeline to
                    // play it on the default audio playback device (using autoaudiosink).
                    let queue =
                        ElementFactory::make("queue", Some("audio_queue")).map_err(|_| MissingElement("queue"))?;
                    let avdec = ElementFactory::make("avdec_opus", Some("avdec_opus"))
                        .map_err(|_| MissingElement("avdec_opus"))?;      
                    let convert = ElementFactory::make("audioconvert", Some("audio_convert"))
                        .map_err(|_| MissingElement("audioconvert"))?;
                    let resample = ElementFactory::make("audioresample", Some("audio_resample"))
                        .map_err(|_| MissingElement("audioresample"))?;
                    let sink = ElementFactory::make("alsasink", Some("audio_alsasink"))
                        .map_err(|_| MissingElement("alsasink"))?;

                    //TODO: check audio device
                    #[cfg(all(target_os= "linux", not(debug_assertions)))]
                    sink.set_property("device", &"micast-wall")?;

                    let elements = &[&queue, &avdec, &convert, &resample, &sink];
                    pipeline.add_many(elements)?;
                    gstreamer::Element::link_many(elements)?;

                    // !!ATTENTION!!:
                    // This is quite important and people forget it often. Without making sure that
                    // the new elements have the same state as the pipeline, things will fail later.
                    // They would still be in Null state and can't process data.
                    for e in elements {
                        e.sync_state_with_parent()?;
                    }

                    queue
                }
            };

            let sink_pad = queue.static_pad("sink").expect("queue has no sinkpad");
            src_pad.link(&sink_pad)?;
            

            // Get the queue element's sink pad and link the decodebin's newly created
            // src pad for the audio stream to it.
            
            
        } else if is_video {
            // Get the queue element's sink pad and link the decodebin's newly created
            // src pad for the video stream to it.
            let sink_pad = videoqueue.static_pad("sink").expect("videoqueue has no sinkpad");
            src_pad.link(&sink_pad)?;
        }

        Ok(())
    };

    // When adding and linking new elements in a callback fails, error information is often sparse.
    // GStreamer's built-in debugging can be hard to link back to the exact position within the code
    // that failed. Since callbacks are called from random threads within the pipeline, it can get hard
    // to get good error information. The macros used in the following can solve that. With the use
    // of those, one can send arbitrary rust types (using the pipeline's bus) into the mainloop.
    // What we send here is unpacked down below, in the iteration-code over sent bus-messages.
    // Because we are using the failure crate for error details here, we even get a backtrace for
    // where the error was constructed. (If RUST_BACKTRACE=1 is set)
    if let Err(err) = insert_sink(is_audio, is_video) {
        // The following sends a message of type Error on the bus, containing our detailed
        // error information.
        element_error!(
            demuxer,
            gstreamer::LibraryError::Failed,
            ("Failed to insert sink"),
            ["{}", err]
        );
    }

}
